package Connections;

import java.io.Closeable;
import java.nio.channels.SelectionKey;

public interface Handler extends Closeable {
    void close();

    void accept(SelectionKey key);
}
