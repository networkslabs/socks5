package Connections;

import message.Message;
import message.messages.HelloMessage;
import message.messages.RequestMessage;
import message.messages.ResponseMessage;
import message.utils.MessageReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;


public class Connection implements Handler {

    private final Logger logger = LoggerFactory.getLogger(Connection.class);


    private static final int BUFFER_SIZE = 4096;
    private final SocketChannel clientChannel;
    private final DNS dns;
    private SocketChannel serverChannel = null;
    private State state = State.HELLO;
    private ByteBuffer readBuff = ByteBuffer.allocateDirect(BUFFER_SIZE);
    private ByteBuffer writeBuff = null;
    private HelloMessage helloMessage = null;
    private RequestMessage requestMessage = null;
    private int EOFcnt = 0;

    public Connection(SocketChannel aux_client, DNS aux_dns, Selector selector) throws IOException {
        logger.info("Opening new connection");
        dns = aux_dns;
        clientChannel = aux_client;
        clientChannel.configureBlocking(false);
        clientChannel.register(selector, SelectionKey.OP_READ, this);
    }

    public SocketChannel getClientChannel() {
        return clientChannel;
    }

    public ByteBuffer getReadBuff() {
        return readBuff;
    }

    public void setReadBuff(ByteBuffer readBuff) {
        this.readBuff = readBuff;
    }

    @Override
    public void accept(SelectionKey key) {
        try {
            if (!key.isValid()) {
                this.close();
                key.cancel();
                return;
            }
            if (key.isReadable()) {
                read(key);
            } else if (key.isWritable()) {
                write(key);
            } else if (key.isConnectable() && key.channel() == serverChannel) {//TODO: проверить селектор: DONE
                serverConnect(key);
            }


        } catch (IOException ex) {
            logger.error("Accepting err: " + ex.getMessage());
            this.close();
        }
    }


    @Override
    public void close() {
        if (clientChannel != null) {
            try {
                clientChannel.close();
            } catch (IOException e) {
                logger.error("Close err", e);
            }
        }
        if (serverChannel != null) {
            try {
                serverChannel.close();
            } catch (IOException e) {
                logger.error("Close err", e);
            }
        }
    }


    private void read(SelectionKey key) throws IOException {
        if (key.channel() == clientChannel) {
            clientRead(key);

        } else if (key.channel() == serverChannel) {
            serverRead(key);
        }
    }


    private void clientRead(SelectionKey key) throws IOException {
        switch (state) {
            case HELLO -> {
                logger.info("Get hello");
                helloMessage = MessageReader.readHelloMessage(this);
                if (helloMessage == null) return;
                key.interestOps(SelectionKey.OP_WRITE);
                readBuff.clear();
            }
            case REQUEST -> {
                logger.info("Get request");
                requestMessage = MessageReader.readRequestMessage(this);
                if (requestMessage == null) return;
                if (!connect()) {
                    serverChannel = null;
                    key.interestOps(SelectionKey.OP_WRITE);
                } else {
                    serverChannel.register(key.selector(), SelectionKey.OP_CONNECT, this);
                    key.interestOps(0);
                }
                readBuff.clear();
            }
            case MESSAGE -> {
                if (this.readFrom(clientChannel, readBuff)) {
                    serverChannel.register(key.selector(), SelectionKey.OP_WRITE, this);
                    key.interestOps(0);
                }
            }
        }

    }

    private void serverRead(SelectionKey key) throws IOException {
        if (readFrom(serverChannel, readBuff)) {
            clientChannel.register(key.selector(), SelectionKey.OP_WRITE, this);
            //TODO: не занулть а убирать чтение: DONE
            key.interestOps(key.interestOps() ^ SelectionKey.OP_READ);
        }
    }

    private void serverConnect(SelectionKey key) throws IOException {
        if (!serverChannel.isConnectionPending()) return;
        if (!serverChannel.finishConnect()) return;//TODO: проверить селектор и посмотреть законектились ли мы: DONE
        //TODO: поставить на чтение: DONE
        key.interestOps(SelectionKey.OP_READ);
        clientChannel.register(key.selector(), SelectionKey.OP_WRITE, this);
    }

    private void write(SelectionKey key) throws IOException {
        if (key.channel() == clientChannel) {
            clientWrite(key);
        } else if (key.channel() == serverChannel) {
            serverWrite(key);
        }
    }

    private void clientWrite(SelectionKey key) throws IOException {
        switch (state) {
            case HELLO -> {
                if (writeBuff == null) {
                    writeBuff = ByteBuffer.wrap(MessageReader.getResponse());
                }
                if (writeTo(clientChannel, writeBuff)) {
                    writeBuff = null;
                    key.interestOps(SelectionKey.OP_READ);
                    state = State.REQUEST;
                    helloMessage = null;
                }
            }
            case REQUEST -> {
                if (writeBuff == null) {
                    Message response = new ResponseMessage(requestMessage, serverChannel != null);
                    writeBuff = ByteBuffer.wrap(response.getData());
                }
                if (writeTo(clientChannel, writeBuff)) {
                    writeBuff = null;
                    if (!requestMessage.isCommand(RequestMessage.CONNECT_TCP) || serverChannel == null) {
                        this.close();
                        logger.info("Not support, please connect TCP");
                    } else {
                        key.interestOps(SelectionKey.OP_READ);
                        serverChannel.register(key.selector(), SelectionKey.OP_READ, this);
                        state = State.MESSAGE;
                    }
                    requestMessage = null;
                }
            }
            case MESSAGE -> {
                if (writeTo(clientChannel, readBuff)) {
                    key.interestOps(SelectionKey.OP_READ);
                    serverChannel.register(key.selector(), SelectionKey.OP_READ, this);
                }
            }
        }

    }

    private void serverWrite(SelectionKey key) throws IOException {
        if (writeTo(serverChannel, readBuff)) {
            key.interestOps(SelectionKey.OP_READ);
            clientChannel.register(key.selector(), SelectionKey.OP_READ, this);
        }
    }


    public boolean connectToServer(InetAddress address) {
        logger.info("Connect with  " + address);
        try {//TODO: проверить селектор и посмотреть законектились ли мы: DONE
            serverChannel.connect(new InetSocketAddress(address, requestMessage.getDestPort()));
        } catch (IOException e) {
            logger.error("Connecting to " + address + " failed", e);
            return false;
        }
        return true;
    }

    private boolean connect() throws IOException {
        serverChannel = SocketChannel.open();
        serverChannel.configureBlocking(false);
        switch (requestMessage.getAddressType()) {
            case RequestMessage.IPv4 -> {
                return connectToServer(InetAddress.getByAddress(requestMessage.getDestAddress()));
            }
            case RequestMessage.IPv6 -> {
                logger.error("IPv6 not support");
                return false;
            }//TODO: кодировку нада: DONE
            case RequestMessage.DOMAIN_NAME -> dns.resolve(
                    new String(requestMessage.getDestAddress(), 0, requestMessage.getDestAddress().length, StandardCharsets.US_ASCII), this);
        }
        return true;
    }

    private boolean readFrom(SocketChannel channel, ByteBuffer buffer) throws IOException {
        buffer.compact();
        int read_bytes = channel.read(buffer);
        if (read_bytes == -1) {//тут какой-то флаг

            if (channel == clientChannel) {
                serverChannel.shutdownOutput();//TODO: если клиент прислал конец потока отправить его серверу: DONE
            } else {
                clientChannel.shutdownOutput();
            }

            EOFcnt++;
            if (EOFcnt == 2)
                this.close();//TODO:не закрывать сокет если второй еще рабоает: DONE

            return false;
        }
        if (read_bytes != 0) {
            buffer.flip();
        }
        return read_bytes != 0;
    }

    private boolean writeTo(SocketChannel channel, ByteBuffer buffer) throws IOException {
        channel.write(buffer);
        return !buffer.hasRemaining();
    }

    private enum State {
        HELLO,
        REQUEST,
        MESSAGE
    }
}
