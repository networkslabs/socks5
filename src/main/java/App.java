import java.io.IOException;

public class App {
    public static void main(String[] args) throws IOException {
        if (args.length < 1) {
            System.err.println("no args");
            return;
        }
        new Proxy(Integer.parseInt(args[0])).run();
    }
}
