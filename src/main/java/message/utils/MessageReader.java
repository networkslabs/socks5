package message.utils;

import Connections.Connection;
import message.Message;
import message.messages.HelloMessage;
import message.messages.RequestMessage;

import java.io.IOException;

public class MessageReader {
    static public HelloMessage readHelloMessage(Connection session) throws IOException { //// читает приветствие от клиента
        /// проверяя на корректность  - вслучае успеха возвращает прочитанное сообщение
        int read_bytes = session.getClientChannel().read(session.getReadBuff());
        if (read_bytes == -1) {
            session.close();
            return null;
        }
        if (HelloMessage.isCorrectSizeOfMessage(session.getReadBuff())) {
            session.setReadBuff(session.getReadBuff().flip());
            return new HelloMessage(session.getReadBuff());
        }
        return null;
    }

    static public RequestMessage readRequestMessage(Connection session) throws IOException {
        int read_bytes = session.getClientChannel().read(session.getReadBuff());
        if (read_bytes == -1) {
            session.close();
            return null;
        }
        if (RequestMessage.isCorrectSizeOfMessage(session.getReadBuff())) {
            session.setReadBuff(session.getReadBuff().flip());
            return new RequestMessage(session.getReadBuff());
        }
        return null;
    }

    static public byte[] getResponse() {
        byte[] data = new byte[2];
        data[0] = Message.SOCKS_5;
        data[1] = Message.NO_AUTHENTICATION;
        return data;
    }
}
